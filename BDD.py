import sqlite3
import logging
from datetime import datetime
import csv


logging.basicConfig(format='%(asctime)s %(message)s', filename=datetime.now().strftime('%Hh %Mmin %Ssec le %d %m %Y.log'), datefmt='%d/%m/%Y %I:%M:%S', level=logging.DEBUG)

def creation(nom):
        connect = sqlite3.connect (nom + ".sqlite3")

        cursor =  connect.cursor()
        cursor.execute("""
        CREATE TABLE IF NOT EXISTS automobile(
                adresse_titulaire TEXT,
                nom TEXT,
                prenom TEXT,
                immatriculation TEXT,
                date_immatriculation DATE,
                vin INTEGER PRIMARY KEY,
                marque TEXT,
                denomination_commerciale TEXT,
                couleur TEXT,
                carosserie INTEGER,
                categorie INTEGER,
                cylindree INTEGER,
                energie INTEGER,
                places INTEGER,
                poids INTEGER,
                puissance INTEGER,
                type TEXT,
                variante INTEGER,
                version INTEGER
        );
        """)


        connect.commit()
        return connect 


def insertion(connecteur,row):
        cursor = connect.cursor()
        cursor.execute ("""
                INSERT into automobile (adresse_titulaire, nom,prenom, immatriculation, date_immatriculation, vin, marque, denomination_commerciale, couleur, carosserie, categorie, cylindree, energie, places, poids, puissance, type, variante, version) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);""",row)
        connect.commit()
        logging.info('ligne insérer')

def ouverturecsv(connecteur):
        cursor = connect.cursor()
        with open ('fichier.csv', 'r') as csvfile :
                reader = csv.reader(csvfile, delimiter = ';')
                for row in reader :
                        cursor.execute("""SELECT vin FROM automobile WHERE vin=? """,(row[5],))
                        test = cursor.fetchall()
                        if(len(test)==0):
                                insertion(connect,row)
                        else:
                                logging.info('ligne deja présente dans la base')


connect = creation("automobile")
ouverturecsv(connect)

connect.close()